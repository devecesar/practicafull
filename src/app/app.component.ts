import { Component } from '@angular/core';
import { PokeapiService } from './pokeapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'practica';
 

  public mostrapoke:any = []


  constructor(private pokeapiservice:PokeapiService ) {

  }

  ngOnInit(): void {
    this.cagarpoke();
  }

  public cagarpoke(){
    this.pokeapiservice.get('https://pokeapi.co/api/v2/pokemon/ditto')
    .subscribe(respuesta => {
      this.mostrapoke = respuesta;

     

   
      console.log(respuesta);
    })

  }
}


